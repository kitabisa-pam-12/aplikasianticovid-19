-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Jun 2020 pada 08.35
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mahasiswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hospital`
--

CREATE TABLE `tbl_hospital` (
  `id` int(11) NOT NULL,
  `nameHospital` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `typeHospital` varchar(50) NOT NULL,
  `kodePos` int(5) NOT NULL,
  `telepon` int(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `faximile` int(13) NOT NULL,
  `website` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_hospital`
--

INSERT INTO `tbl_hospital` (`id`, `nameHospital`, `location`, `typeHospital`, `kodePos`, `telepon`, `email`, `faximile`, `website`) VALUES
(1, 'Rumah Sakit Pematang Siantar', 'Jl. Sutomo No.230 P. Siantar ', 'Rumah Sakit Umum', 22345, 63421780, 'rsu@gmail.com', 89567, 'https://rumahsakitumumpematangsiantar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_puskesmas`
--

CREATE TABLE `tbl_puskesmas` (
  `id` int(11) NOT NULL,
  `namaPuskesmas` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `faximile` varchar(100) NOT NULL,
  `kepalaPuskesmas` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_puskesmas`
--

INSERT INTO `tbl_puskesmas` (`id`, `namaPuskesmas`, `location`, `telepon`, `faximile`, `kepalaPuskesmas`, `alamat`, `email`) VALUES
(2, 'Puskesmas porsea', 'Porsea, Kab.Toba', '062541010', '22384', 'dr.cinta', 'Jalan Patuan Nagari', 'yantil@gmail.com'),
(3, 'PuskesmasPorsea', 'Porsea', '0822727277', '09888', 'dr.vetra', 'jalan.patuan nagari', 'vetra@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_survei`
--

CREATE TABLE `tbl_survei` (
  `question` varchar(100) NOT NULL,
  `optA` varchar(5) NOT NULL,
  `optB` varchar(5) NOT NULL,
  `optC` varchar(5) NOT NULL,
  `answer` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_survei`
--

INSERT INTO `tbl_survei` (`question`, `optA`, `optB`, `optC`, `answer`) VALUES
('Saya berjabat tangan dengan orang lain', 'Ya', 'Tidak', 'Both', 'Ya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nama`, `email`, `password`) VALUES
(2, 'grace', 'grace@gmail.com', 'grace\n\n\n');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_hospital`
--
ALTER TABLE `tbl_hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_puskesmas`
--
ALTER TABLE `tbl_puskesmas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_hospital`
--
ALTER TABLE `tbl_hospital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_puskesmas`
--
ALTER TABLE `tbl_puskesmas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

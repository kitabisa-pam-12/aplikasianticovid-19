<?php 
require __DIR__ . '/vendor/autoload.php';
require 'libs/NotORM.php'; 

use \Slim\App;

$app = new App();

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'db_mahasiswa';
$dbmethod = 'mysql:dbname=';

$dsn = $dbmethod.$dbname;
$pdo = new PDO($dsn, $dbuser, $dbpass);
$db  = new NotORM($pdo);

$app-> get('/', function(){
    echo "API Mahasiswa";
});



$app ->get('/semuapertanyaan', function() use($app, $db){
	$survei["error"] = false;
	$survei["message"] = "Berhasil mendapatkan data pertanyaan";
    foreach($db->tbl_survei() as $data){
        $survei['semuapertanyaan'][] = array(
            'question' => $data['question'],
            'optA' => $data['optA'],
			'optB' => $data['optB'],
			'optC' => $data['optC'],
			'answer' => $data['optA']
            );
    }
    echo json_encode($survei);
});

$app ->get('/puskesmas', function() use($app, $db){
    if ($db->tbl_puskesmas()->count() == 0) {
        $responseJson["error"] = true;
        $responseJson["message"] = "Belum ada data puskesmas";
    } else {
        $responseJson["error"] = false;
        $responseJson["message"] = "Berhasil mendapatkan data puskesmas";
        foreach($db->tbl_puskesmas() as $data){
        $responseJson['semuapuskesmas'][] = array(
            'id' => $data['id'],
            'namaPuskesmas' => $data['namaPuskesmas'],
            'location' => $data['location'],
			'telepon' => $data['telepon'],
            'faximile' => $data['faximile'],
            'kepalaPuskesmas' => $data['kepalaPuskesmas'],
			'alamat' => $data['alamat'],
            'email' => $data['email']
            );
        }
    }
    echo json_encode($responseJson);
});



$app->post('/puskesmas', function($request, $response, $args) use($app, $db){
    $puskesmas = $request->getParams();
    $result = $db->tbl_puskesmas->insert($puskesmas);

    $responseJson["error"] = false;
    $responseJson["message"] = "Berhasil menambahkan ke database";
    echo json_encode($responseJson);
});


$app->delete('/puskesmas/{id}', function($request, $response, $args) use($app, $db){
    $puskesmas = $db->tbl_puskesmas()->where('id', $args);
    if($puskesmas->fetch()){
        $result = $puskesmas->delete();
        echo json_encode(array(
            "error" => false,
            "message" => "Data berhasil dihapus"));
    }
    else{
        echo json_encode(array(
            "error" => true,
            "message" => "Puskesmas ID tersebut tidak ada"));
    }
});

$app ->get('/hospital', function() use($app, $db){
    if ($db->tbl_hospital()->count() == 0) {
        $responseJson["error"] = true;
        $responseJson["message"] = "Belum ada data hospital";
    } else {
        $responseJson["error"] = false;
        $responseJson["message"] = "Berhasil mendapatkan data hospital";
        foreach($db->tbl_hospital() as $data){
        $responseJson['semuahospital'][] = array(
            'id' => $data['id'],
            'nameHospital' => $data['nameHospital'],
            'location' => $data['location'],
            'typeHospital' => $data['typeHospital'],
			'kodePos' => $data['kodePos'],
			'telepon' => $data['telepon'],
            'email' => $data['email'],
            'faximile' => $data['faximile'],
            'website' => $data['website']
            );
        }
    }
    echo json_encode($responseJson);
});



$app->post('/hospital', function($request, $response, $args) use($app, $db){
    $hospital = $request->getParams();
    $result = $db->tbl_hospital->insert($hospital);

    $responseJson["error"] = false;
    $responseJson["message"] = "Berhasil menambahkan ke database";
    echo json_encode($responseJson);
});

$app->delete('/hospital/{id}', function($request, $response, $args) use($app, $db){
    $hospital = $db->tbl_hospital()->where('id', $args);
    if($hospital->fetch()){
        $result = $hospital->delete();
        echo json_encode(array(
            "error" => false,
            "message" => "Data berhasil dihapus"));
    }
    else{
        echo json_encode(array(
            "error" => true,
            "message" => "Puskesmas ID tersebut tidak ada"));
    }
});

$app->post('/user', function($request, $response, $args) use($app, $db){
    $user = $request->getParams();
    $result = $db->tbl_user->insert($user);

    $responseJson["error"] = false;
    $responseJson["message"] = "Berhasil menambahkan ke database";
    echo json_encode($responseJson);
});


$app ->get('/user', function() use($app, $db){
    if ($db->tbl_user()->count() == 0) {
        $responseJson["error"] = true;
        $responseJson["message"] = "Belum ada data user";
    } else {
        $responseJson["error"] = false;
        $responseJson["message"] = "Berhasil mendapatkan data user";
        foreach($db->tbl_user() as $data){
        $responseJson['semuauser'][] = array(
            'id' => $data['id'],
            'nama' => $data['nama'],
			'email' => $data['email']
            );
        }
    }
    echo json_encode($responseJson);
});
//run App
$app->run();
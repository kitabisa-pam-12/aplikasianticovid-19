package com.develops.groupproject.Models;

public class ModelDataIndonesia {
    private  String name;
    private  String positif;
    private  String sembuh;
    private  String meninggal;
    private  String dirawat;


    public String getName() {
        return name;
    }

    public String getPositif() {
        return positif;
    }

    public String getSembuh() {
        return sembuh;
    }

    public String getMeninggal() {
        return meninggal;
    }

    public String getDirawat() {
        return dirawat;
    }
}

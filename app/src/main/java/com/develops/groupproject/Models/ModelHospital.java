package com.develops.groupproject.Models;

import com.google.gson.annotations.SerializedName;

public class ModelHospital {

    @SerializedName("id")
    private String id;

    @SerializedName("nameHospital")
    private String nameHospital;

    @SerializedName("location")
    private String location;

    @SerializedName("typeHospital")
    private String typeHospital;

    @SerializedName("kodePos")
    private String kodePos;

    @SerializedName("telepon")
    private String telepon;

    @SerializedName("email")
    private String email;

    @SerializedName("faximile")
    private String faximile;

    @SerializedName("website")
    private String website;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameHospital() {
        return nameHospital;
    }

    public void setNameHospital(String nameHospital) {
        this.nameHospital = nameHospital;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTypeHospital() {
        return typeHospital;
    }

    public void setTypeHospital(String typeHospital) {
        this.typeHospital = typeHospital;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFaximile() {
        return faximile;
    }

    public void setFaximile(String faximile) {
        this.faximile = faximile;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "ModelHospital{" +
                "id='" + id + '\'' +
                ", nameHospital='" + nameHospital + '\'' +
                ", location='" + location + '\'' +
                ", typeHospital='" + typeHospital + '\'' +
                ", kodePos='" + kodePos + '\'' +
                ", telepon='" + telepon + '\'' +
                ", email='" + email + '\'' +
                ", faximile='" + faximile + '\'' +
                ", website='" + website + '\'' +
                '}';
    }
}

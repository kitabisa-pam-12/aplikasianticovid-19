package com.develops.groupproject.Models;

import com.google.gson.annotations.SerializedName;


public class ModelPuskesmas {

    @SerializedName("id")
    private String id;

    @SerializedName("namaPuskesmas")
    private String namaPuskesmas;

    @SerializedName("location")
    private String location;

    @SerializedName("telepon")
    private String telepon;

    @SerializedName("faximile")
    private String faximile;

    @SerializedName("kepalaPuskesmas")
    private String kepalaPuskesmas;

    @SerializedName("alamat")
    private String alamat;

    @SerializedName("email")
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNamaPuskesmas() {
        return namaPuskesmas;
    }

    public void setNamaPuskesmas(String namaPuskesmas) {
        this.namaPuskesmas = namaPuskesmas;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getFaximile() {
        return faximile;
    }

    public void setFaximile(String faximile) {
        this.faximile = faximile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKepalaPuskesmas() {
        return kepalaPuskesmas;
    }

    public void setKepalaPuskesmas(String kepalaPuskesmas) {
        this.kepalaPuskesmas = kepalaPuskesmas;
    }

    @Override
    public String toString() {
        return "ModelPuskesmas{" +
                "id='" + id + '\'' +
                ", namaPuskesmas='" + namaPuskesmas + '\'' +
                ", location='" + location + '\'' +
                ", telepon='" + telepon + '\'' +
                ", faximile='" + faximile + '\'' +
                ", kepalaPuskesmas='" + kepalaPuskesmas + '\'' +
                ", alamat='" + alamat + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

package com.develops.groupproject.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseHospital {

    @SerializedName("semuapuskesmas")
    private List<ModelHospital> modelHospital;

    @SerializedName("error")
    private boolean error;

    @SerializedName("message")
    private String message;

    public List<ModelHospital> getModelHospital() {
        return modelHospital;
    }

    public void setModelHospital(List<ModelHospital> modelHospital) {
        this.modelHospital = modelHospital;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseHospital{" +
                "modelHospital=" + modelHospital +
                ", error=" + error +
                ", message='" + message + '\'' +
                '}';
    }
}

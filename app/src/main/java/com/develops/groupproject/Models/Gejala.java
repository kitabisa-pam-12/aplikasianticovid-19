package com.develops.groupproject.Models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "gejala_table")
public class Gejala {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private String id;


    @Nullable
    @ColumnInfo(name = "title")
    private String title;

    @Nullable
    @ColumnInfo(name = "keterangan")
    private String keterangan;

    public Gejala(@NonNull String id, @Nullable String title, @Nullable String keterangan) {
        this.id = id;
        this.title = title;
        this.keterangan = keterangan;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    public void setTitle(@Nullable String title) {
        this.title = title;
    }

    @Nullable
    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(@Nullable String keterangan) {
        this.keterangan = keterangan;
    }
}
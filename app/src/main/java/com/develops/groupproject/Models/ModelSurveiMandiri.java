package com.develops.groupproject.Models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "survei_table")
public class ModelSurveiMandiri {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public int id;

    @ColumnInfo(name = "nik")
    public String nik;

    @ColumnInfo(name = "nama")
    public String nama;

    @ColumnInfo(name = "ttl")
    public String ttl;

    @ColumnInfo(name = "gender")
    public String gender;

    @ColumnInfo(name = "alamat")
    public String alamat;

    @ColumnInfo(name = "kontak")
    public String  kontak;

    @ColumnInfo(name = "email")
    public String email;


    public ModelSurveiMandiri(String nik, String nama, String ttl, String gender, String alamat, String kontak, String email) {
        this.nik = nik;
        this.nama = nama;
        this.ttl = ttl;
        this.gender = gender;
        this.alamat = alamat;
        this.kontak = kontak;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ModelSurveiMandiri{" +
                "id=" + id +
                ", nik='" + nik + '\'' +
                ", nama='" + nama + '\'' +
                ", ttl='" + ttl + '\'' +
                ", gender='" + gender + '\'' +
                ", alamat='" + alamat + '\'' +
                ", kontak='" + kontak + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

package com.develops.groupproject.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePuskesmas {

    @SerializedName("semuapuskesmas")
    private List<ModelPuskesmas> modelPuskesmas;

    @SerializedName("error")
    private boolean error;

    @SerializedName("message")
    private String message;

    public List<ModelPuskesmas> getModelPuskesmas() {
        return modelPuskesmas;
    }

    public List<ModelPuskesmas> getSemuamatkul() {
        return modelPuskesmas;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return
                "ResponsePuskesmas{" +
                        "modelpuskesmas = '" + modelPuskesmas + '\'' +
                        ",error = '" + error + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}

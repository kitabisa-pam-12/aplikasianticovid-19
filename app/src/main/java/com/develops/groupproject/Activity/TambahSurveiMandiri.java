package com.develops.groupproject.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.develops.groupproject.Models.ModelSurveiMandiri;
import com.develops.groupproject.R;

public class TambahSurveiMandiri extends AppCompatActivity {


    public static final  String EXTRA_ID="com.develops.groupproject.Activity.ID";
    public static final  String EXTRA_NIK="com.develops.groupproject.Activity.NIK";
    public static final  String EXTRA_NAMA="com.develops.groupproject.Activity.NAMA";
    public static final  String EXTRA_TTL="com.develops.groupproject.Activity.TTL";
    public static final  String EXTRA_ALAMAT="com.develops.groupproject.Activity.ALAMAT";
    public static final  String EXTRA_KONTAK="com.develops.groupproject.Activity.KONATK";
    public static final  String EXTRA_EMAIL="com.develops.groupproject.Activity.EMAIL";
    public static final  String EXTRA_GENDER="com.develops.groupproject.Activity.GENDER";


    EditText  edt_nik, edt_nama, edt_ttl, edt_gender, edt_alamat, edt_kontak, edt_email;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_survei_mandiri);

        edt_nik=(EditText)findViewById(R.id.edt_nik);
        edt_nama=(EditText)findViewById(R.id.edt_nama);
        edt_ttl=(EditText)findViewById(R.id.edt_ttl);
        edt_alamat=(EditText)findViewById(R.id.edt_alamat);
        edt_kontak=(EditText)findViewById(R.id.edt_kontak);
        edt_email=(EditText)findViewById(R.id.edt_email);
        edt_gender=(EditText)findViewById(R.id.edt_gender);


        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)){
            setTitle("Edit Data");
            edt_nik.setText(intent.getStringExtra(EXTRA_NIK));
            edt_nama.setText(intent.getStringExtra(EXTRA_NAMA));
            edt_ttl.setText(intent.getStringExtra(EXTRA_TTL));
            edt_alamat.setText(intent.getStringExtra(EXTRA_ALAMAT));
            edt_kontak.setText(intent.getStringExtra(EXTRA_KONTAK));
            edt_email.setText(intent.getStringExtra(EXTRA_EMAIL));
            edt_gender.setText(intent.getStringExtra(EXTRA_GENDER));
        }else{
            setTitle("Tambah Data");
        }


    }

    @Override
    public  boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_data, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.save:
                savedata();
                return true;
                default:
                    return super.onOptionsItemSelected(item);
        }
    }

    private void savedata() {
        String nik = edt_nik.getText().toString();
        String nama = edt_nama.getText().toString();
        String ttl= edt_ttl.getText().toString();
        String alamat = edt_alamat.getText().toString();
        String kontak = edt_kontak.getText().toString();
        String email = edt_email.getText().toString();
        String gender = edt_gender.getText().toString();

        if(nik.trim().isEmpty()||nama.trim().isEmpty()||
        ttl.trim().isEmpty()||alamat.trim().isEmpty()||kontak.trim().isEmpty()||email.trim().isEmpty()
        ||gender.trim().isEmpty()){
            Toast.makeText(this,"Masih ada data yang kosong", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NIK, nik);
        data.putExtra(EXTRA_NAMA, nama);
        data.putExtra(EXTRA_TTL, ttl);
        data.putExtra(EXTRA_ALAMAT, alamat);
        data.putExtra(EXTRA_KONTAK, kontak);
        data.putExtra(EXTRA_EMAIL, email);
        data.putExtra(EXTRA_GENDER, gender);

        int id =  getIntent().getIntExtra(EXTRA_ID,1);
        if (id != -1){
            data.putExtra(EXTRA_ID,id);
        }

        setResult(RESULT_OK, data);
        finish();



    }
}

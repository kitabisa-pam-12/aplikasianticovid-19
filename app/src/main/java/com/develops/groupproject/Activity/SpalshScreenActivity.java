package com.develops.groupproject.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.develops.groupproject.R;

import static java.lang.Thread.sleep;


public class SpalshScreenActivity extends AppCompatActivity {


    private final static int EXIT_CODE = 100;

    ImageView imageViewSplas;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh_screen);


       imageViewSplas=findViewById(R.id.image);
       textView=findViewById(R.id.text);


        Animation animation = AnimationUtils.loadAnimation(this,R.anim.transition);
        imageViewSplas.setAnimation(animation);
        textView.setAnimation(animation);



        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {


                try {
                    sleep(3000);

                }catch (Exception e)
                {
                    e.printStackTrace();

                }finally {

                    GoPlayActivity();
                }



            }
        });
        thread.start();


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EXIT_CODE){

            if (resultCode == RESULT_OK){
                if (data.getBooleanExtra("EXIT",true)){
                    finish();
                }

            }
        }
    }

    private void GoPlayActivity() {

        startActivityForResult(new Intent(SpalshScreenActivity.this,LoginActivity.class),EXIT_CODE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }


}

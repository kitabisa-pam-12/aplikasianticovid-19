package com.develops.groupproject.Activity;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.develops.groupproject.Models.Gejala;
import com.develops.groupproject.Repositori.GejalaRepository;

import java.util.List;

/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all words.
 */

public class GejalaViewModel extends AndroidViewModel {

    private GejalaRepository mRepository;
    private LiveData<List<Gejala>> mAllWords;

    public GejalaViewModel(Application application) {
        super(application);
        mRepository = new GejalaRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<Gejala>> getAllWords() { return mAllWords; }

    public void insert(Gejala gejala) { mRepository.insert(gejala); }
}
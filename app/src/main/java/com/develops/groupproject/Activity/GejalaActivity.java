package com.develops.groupproject.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Adapter.GejalaListAdapter;
import com.develops.groupproject.Models.Gejala;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.SharedPrefManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class GejalaActivity extends AppCompatActivity {

    public static final int NEW_GEJALA_ACTIVITY_REQUEST_CODE = 1;


    private RecyclerView recyclerView;
    private GejalaViewModel mGejalaViewModel;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gejala);

        sharedPrefManager = new SharedPrefManager(this);
        recyclerView = findViewById(R.id.recyclerview);
        final GejalaListAdapter adapter = new GejalaListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Get a new or existing ViewModel from the ViewModelProvider.
        mGejalaViewModel = new ViewModelProvider(this).get(GejalaViewModel.class);

        mGejalaViewModel.getAllWords().observe(this, new Observer<List<Gejala>>() {
            @Override
            public void onChanged(@Nullable final List<Gejala> gejalas) {
                // Update the cached copy of the gejalas in the adapter.
                adapter.setWords(gejalas);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GejalaActivity.this, NewGejalaActivity.class);
                startActivityForResult(intent, NEW_GEJALA_ACTIVITY_REQUEST_CODE);
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
            startActivity(new Intent(GejalaActivity.this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_GEJALA_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Gejala gejala = new Gejala(data.getStringExtra(NewGejalaActivity.NOMOR_GEJALA),  data.getStringExtra(NewGejalaActivity.TITLE_GEJALA),
                    data.getStringExtra(NewGejalaActivity.KETERANGAN));
            mGejalaViewModel.insert(gejala);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
}

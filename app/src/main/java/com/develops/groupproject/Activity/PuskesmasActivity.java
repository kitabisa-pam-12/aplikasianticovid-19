package com.develops.groupproject.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Adapter.PuskesmasAdapter;
import com.develops.groupproject.Database.Service;
import com.develops.groupproject.Models.ModelPuskesmas;
import com.develops.groupproject.Models.ResponsePuskesmas;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.Constant;
import com.develops.groupproject.Util.RecyclerItemClickListener;
import com.develops.groupproject.Util.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PuskesmasActivity extends AppCompatActivity {

    Button btnTambahPuskesmas;
    RecyclerView rvPuskesmas;
    TextView tvPuskesmas;
    ProgressDialog loading;

    Context context;
    List<ModelPuskesmas> modelPuskesmasList= new ArrayList<>();
    PuskesmasAdapter puskesmasAdapter;
    Service service;

    @Override



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puskesmas);

        btnTambahPuskesmas = (Button) findViewById(R.id.btnTambahPuskesmas);
        rvPuskesmas = (RecyclerView) findViewById(R.id.rvPuskesmas);
        tvPuskesmas = (TextView) findViewById(R.id.tvNotFound);

        service = UtilsApi.getAPIService();
        context = this;

        puskesmasAdapter = new PuskesmasAdapter(this, modelPuskesmasList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvPuskesmas.setLayoutManager(layoutManager);
        rvPuskesmas.setItemAnimator(new DefaultItemAnimator());

        getDataPuskesmas();


        btnTambahPuskesmas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PuskesmasActivity.this, TambahPuskesmasActivity2.class));
            }
        });
    }


    private void getDataPuskesmas() {

        loading = ProgressDialog.show(context, null, "Harap Tunggu", true, false);

        service.getSemuaPuskesmas().enqueue(new Callback<ResponsePuskesmas>() {
            @Override
            public void onResponse(Call<ResponsePuskesmas> call, Response<ResponsePuskesmas> response) {
                if (response.isSuccessful()){
                    loading.dismiss();
                    if (response.body().isError()){
                        tvPuskesmas.setVisibility(View.VISIBLE);
                    }
                    else {
                        final List<ModelPuskesmas> modelPuskesmas = response.body().getModelPuskesmas();
                        rvPuskesmas.setAdapter(new PuskesmasAdapter(context, modelPuskesmas));
                        puskesmasAdapter.notifyDataSetChanged();

                        initDataIntent(modelPuskesmas);
                    }
                }else{
                    loading.dismiss();
                    Toast.makeText(context, "Gagal mengambil database puskesmas", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponsePuskesmas> call, Throwable t) {

                loading.dismiss();
                Toast.makeText(context,"koneksi internet bermasalah", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initDataIntent(final List<ModelPuskesmas> puskesmasList) {

        rvPuskesmas.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                String id= puskesmasList.get(position).getId();
                String nama_puskesmas = puskesmasList.get(position).getNamaPuskesmas();
                String location= puskesmasList.get(position).getLocation();
                String telepon = puskesmasList.get(position).getTelepon();
                String faximile = puskesmasList.get(position).getFaximile();
                String kepalaPuskesmas = puskesmasList.get(position).getKepalaPuskesmas();
                String alamat= puskesmasList.get(position).getAlamat();
                String email = puskesmasList.get(position).getEmail();

                Intent detail= new Intent(context, PuskesmasDetailActivity.class);
                detail.putExtra(Constant.KEY_ID, id);
                detail.putExtra(Constant.KEY_NAMA_PUSKESMAS, nama_puskesmas);
                detail.putExtra(Constant.KEY_LOCATION, location);
                detail.putExtra(Constant.KEY_TELEPON, telepon);
                detail.putExtra(Constant.KEY_FAXIMILE, faximile);
                detail.putExtra(Constant.KEY_KEPALA_PUSKESMAS, kepalaPuskesmas);
                detail.putExtra(Constant.KEY_ALAMAT, alamat);
                detail.putExtra(Constant.KEY_EMAIL, email);
                startActivity(detail);

            }
                }));
    }
}

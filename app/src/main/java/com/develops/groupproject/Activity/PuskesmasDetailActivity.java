package com.develops.groupproject.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.develops.groupproject.Database.Service;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.Constant;
import com.develops.groupproject.Util.UtilsApi;

import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PuskesmasDetailActivity  extends AppCompatActivity {

    TextView tvNamaKepala, tvTelepon, tvFaximile, tvEmail;
    ProgressDialog loading;
    Button btnHapus;

    String mKepalaPuskesmas;
    String mTelepon;
    String mFaximile;
    String mEmail, mId, mNama;

    Context context;
    Service service;

    public String[] mColors = {
            "#39add1",
            "#3079ab",
            "#c25975",
            "#e15258",
            "#f9845b",
            "#838cc7",
            "#7d669e",
            "#53bbb4",
            "#51b46d",
            "#e0ab18",
            "#637a91",
            "#f092b0",
            "#b7c0c7"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_puskesmas);

        tvNamaKepala = (TextView) findViewById(R.id.txtNamaKepala);
        tvTelepon = (TextView) findViewById(R.id.txtNoTlp);
        tvFaximile = (TextView) findViewById(R.id.txtFax);
        tvEmail = (TextView) findViewById(R.id.txtEmail);
        btnHapus=(Button) findViewById(R.id.btnHapus);
        context = this;
        service= UtilsApi.getAPIService();


        Intent intent = getIntent();
        mId= intent.getStringExtra(Constant.KEY_ID);;
        mKepalaPuskesmas = intent.getStringExtra(Constant.KEY_KEPALA_PUSKESMAS);
        mTelepon = intent.getStringExtra(Constant.KEY_TELEPON);
        mFaximile = intent.getStringExtra(Constant.KEY_FAXIMILE);
        mEmail = intent.getStringExtra(Constant.KEY_EMAIL);



        tvNamaKepala.setText(mKepalaPuskesmas);
        tvTelepon.setText(mTelepon);
        tvFaximile.setText(mFaximile);
        tvEmail.setText(mEmail);


        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDeletePuskesmas();
            }
        });
    }

    private void requestDeletePuskesmas() {

        loading = ProgressDialog.show(context, null, "Harap Tunggu...", true, false);

        service.detelePuskesmas(mId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    loading.dismiss();
                    Toast.makeText(context, "Berhasil mengapus Data", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(context, PuskesmasActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                } else {
                    loading.dismiss();
                    Toast.makeText(context, "Gagal menghapus data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(context, "koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }
    }






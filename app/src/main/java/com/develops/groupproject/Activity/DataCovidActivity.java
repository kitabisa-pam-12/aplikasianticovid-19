package com.develops.groupproject.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.develops.groupproject.Models.ModelDataIndonesia;
import com.develops.groupproject.Models.User;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.Api;
import com.develops.groupproject.Util.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataCovidActivity  extends AppCompatActivity {

    ImageButton btnGejala, btnInfoRS, btnSurvei;
    private TextView tvUser;
    private User user;
    ImageView imgGejala,imgInfoRS,imgSurvei,imgCekResiko;
    TextView tPositif, tSembuh, tMeninggal, tDirawat;


    SharedPrefManager sharedPrefManager;
    ProgressDialog dialog;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_covid1);


        user = (User) getIntent().getSerializableExtra("User");

        tvUser = findViewById(R.id.tvUser);

        if (user != null) {
            tvUser.setText("Hallo " + user.getName() +" "+ user.getLastName());
        }

        imgGejala = findViewById(R.id.imageView12);
        imgInfoRS = findViewById(R.id.imageView13);
        imgSurvei = findViewById(R.id.imageView14);
        imgCekResiko=findViewById(R.id.imageView15);

        sharedPrefManager = new SharedPrefManager(this);
        tPositif = findViewById(R.id.tPositif);
        tSembuh = findViewById(R.id.tsembuh);
        tMeninggal = findViewById(R.id.tMeninggal);
        tDirawat = findViewById(R.id.tDirawat);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading");
        dialog.setCancelable(false);
        dialog.show();

        Call<List<ModelDataIndonesia>> call = Api.service().getData();
        call.enqueue(new Callback<List<ModelDataIndonesia>>() {


            @Override
            public void onResponse(Call<List<ModelDataIndonesia>> call, Response<List<ModelDataIndonesia>> response) {

                tPositif.setText(response.body().get(0).getPositif());
                tSembuh.setText(response.body().get(0).getSembuh());
                tMeninggal.setText(response.body().get(0).getMeninggal());
                tDirawat.setText(response.body().get(0).getDirawat());
                dialog.cancel();
            }

            @Override
            public void onFailure(Call<List<ModelDataIndonesia>> call, Throwable t) {

                Toast.makeText(DataCovidActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        imgGejala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DataCovidActivity.this, GejalaActivity.class));
            }
        });


        imgInfoRS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DataCovidActivity.this, RumahSakitActivity.class));
            }
        });

        imgSurvei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DataCovidActivity.this, SurveiActivity.class));
            }
        });




    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
            startActivity(new Intent(DataCovidActivity.this, LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

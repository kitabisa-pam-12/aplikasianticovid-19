package com.develops.groupproject.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.develops.groupproject.Adapter.SurveiAdapter;
import com.develops.groupproject.Adapter.SurveiViewModel;
import com.develops.groupproject.Models.ModelSurveiMandiri;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.SharedPrefManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class SurveiActivity extends AppCompatActivity {

    public static final  int ADD_DATA=1;
    public static final  int EDIT_DATA=2;
    private SurveiViewModel surveiViewModel;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survei);

        setTitle(" Data Survei Mandiri");

        FloatingActionButton button = findViewById(R.id.button_add);
        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SurveiActivity.this, TambahSurveiMandiri.class);
                startActivityForResult(intent, ADD_DATA);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final SurveiAdapter adapter = new SurveiAdapter();
        recyclerView.setAdapter(adapter);

        surveiViewModel =  new ViewModelProvider(this).get(SurveiViewModel.class);
        surveiViewModel.getAllData().observe(this, new Observer<List<ModelSurveiMandiri>>() {
            @Override
            public void onChanged(@Nullable  List<ModelSurveiMandiri> modelSurveiMandiris) {
                adapter.setSurvei(modelSurveiMandiris);
            }
        });

        new  ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove( RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped( RecyclerView.ViewHolder viewHolder, int direction) {
                surveiViewModel.deleteTask(adapter.getSurvei(viewHolder.getAdapterPosition()));
                Toast.makeText(SurveiActivity.this, "Data telah dihapus", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);


        adapter.setOnItemClickListener(new SurveiAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ModelSurveiMandiri surveiMandiri) {
                Intent intent = new Intent(SurveiActivity.this, TambahSurveiMandiri.class);
                intent.putExtra(TambahSurveiMandiri.EXTRA_ID, surveiMandiri.getId());
                intent.putExtra(TambahSurveiMandiri.EXTRA_NIK, surveiMandiri.getNik());
                intent.putExtra(TambahSurveiMandiri.EXTRA_NAMA, surveiMandiri.getNama());
                intent.putExtra(TambahSurveiMandiri.EXTRA_ALAMAT, surveiMandiri.getAlamat());
                intent.putExtra(TambahSurveiMandiri.EXTRA_TTL, surveiMandiri.getTtl());
                intent.putExtra(TambahSurveiMandiri.EXTRA_EMAIL, surveiMandiri.getEmail());
                intent.putExtra(TambahSurveiMandiri.EXTRA_KONTAK, surveiMandiri.getKontak());
                intent.putExtra(TambahSurveiMandiri.EXTRA_GENDER, surveiMandiri.getGender());
                startActivityForResult(intent, EDIT_DATA);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_DATA && resultCode== RESULT_OK) {
            String nik = data.getStringExtra(TambahSurveiMandiri.EXTRA_NIK);
            String nama = data.getStringExtra(TambahSurveiMandiri.EXTRA_NAMA);
            String ttl = data.getStringExtra(TambahSurveiMandiri.EXTRA_ALAMAT);
            String alamat = data.getStringExtra(TambahSurveiMandiri.EXTRA_TTL);
            String email = data.getStringExtra(TambahSurveiMandiri.EXTRA_EMAIL);
            String kontak = data.getStringExtra(TambahSurveiMandiri.EXTRA_KONTAK);
            String gender = data.getStringExtra(TambahSurveiMandiri.EXTRA_GENDER);


            ModelSurveiMandiri surveiMandiri = new ModelSurveiMandiri(nik, nama, ttl, alamat, email, kontak, gender);
            surveiViewModel.insertTask(surveiMandiri);

            Toast.makeText(this, "Data Tersimpan", Toast.LENGTH_SHORT).show();
        }else if(requestCode == EDIT_DATA && resultCode == RESULT_OK){
            int id = data.getIntExtra(TambahSurveiMandiri.EXTRA_ID, 1);

            if (id == -1){
                Toast.makeText(this, "Data tidak dapat di update", Toast.LENGTH_SHORT).show();
                return;
            }

            String nik = data.getStringExtra(TambahSurveiMandiri.EXTRA_NIK);
            String nama = data.getStringExtra(TambahSurveiMandiri.EXTRA_NAMA);
            String ttl = data.getStringExtra(TambahSurveiMandiri.EXTRA_ALAMAT);
            String alamat = data.getStringExtra(TambahSurveiMandiri.EXTRA_TTL);
            String email = data.getStringExtra(TambahSurveiMandiri.EXTRA_EMAIL);
            String kontak = data.getStringExtra(TambahSurveiMandiri.EXTRA_KONTAK);
            String gender = data.getStringExtra(TambahSurveiMandiri.EXTRA_GENDER);

            ModelSurveiMandiri surveiMandiri = new ModelSurveiMandiri(nik, nama, ttl, alamat, email, kontak, gender);
            surveiMandiri.setId(id);
            surveiViewModel.updateTask(surveiMandiri);

            Toast.makeText(this, "Data berhasil diupdate", Toast.LENGTH_SHORT).show();

        }else {
            Toast.makeText(this, "Data Tidak Tersimpan", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_survei, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete:
                surveiViewModel.deeleteAllData();
                Toast.makeText(this, "Semua Data telah terhapus", Toast.LENGTH_SHORT).show();
                return true;

            case  R.id.action_settings:
                sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                startActivity(new Intent(SurveiActivity.this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                return true;
                default:
                     }

        return super.onOptionsItemSelected(item);

    }
}

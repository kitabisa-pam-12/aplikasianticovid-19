package com.develops.groupproject.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Adapter.HospitalKhususAdapter;
import com.develops.groupproject.Database.Service;
import com.develops.groupproject.Models.ModelHospital;
import com.develops.groupproject.Models.ModelPuskesmas;
import com.develops.groupproject.Models.ResponseHospital;
import com.develops.groupproject.Models.ResponsePuskesmas;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.Constant;
import com.develops.groupproject.Util.RecyclerItemClickListener;
import com.develops.groupproject.Util.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HospitalKhususActivity extends AppCompatActivity {

    Button btnTambahHospital;
    RecyclerView rvHospital;
    TextView tvHospital;
    ProgressDialog loading;

    Context context;
    List<ModelHospital> modelHospitalList= new ArrayList<>();
    HospitalKhususAdapter hospitalAdapter;
    Service service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital);

        btnTambahHospital = (Button) findViewById(R.id.btnTambahHospital);
        rvHospital = (RecyclerView) findViewById(R.id.rvRS);
        tvHospital = (TextView) findViewById(R.id.tvNotFound);

        service = UtilsApi.getAPIService();
        context = this;

        hospitalAdapter = new HospitalKhususAdapter(this, modelHospitalList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvHospital.setLayoutManager(layoutManager);
        rvHospital.setItemAnimator(new DefaultItemAnimator());

        getDataHospital();


        btnTambahHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HospitalKhususActivity.this, TambahPuskesmasActivity2.class));
            }
        });
    }


    private void getDataHospital() {

        loading = ProgressDialog.show(context, null, "Harap Tunggu", true, false);

        service.getSemuaHospital().enqueue(new Callback<ResponseHospital>() {
            @Override
            public void onResponse(Call<ResponseHospital> call, Response<ResponseHospital> response) {
                if (response.isSuccessful()){
                    loading.dismiss();
                    if (response.body().isError()){
                        tvHospital.setVisibility(View.VISIBLE);
                    }
                    else {
                        final List<ModelHospital> modelHospital = response.body().getModelHospital();
                        rvHospital.setAdapter(new HospitalKhususAdapter(context, modelHospital));
                        hospitalAdapter.notifyDataSetChanged();

                        initDataIntent(modelHospital);
                    }
                }else{
                    loading.dismiss();
                    Toast.makeText(context, "Gagal mengambil database puskesmas", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseHospital> call, Throwable t) {

                loading.dismiss();
                Toast.makeText(context,"koneksi internet bermasalah", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initDataIntent(final List<ModelHospital> hospitalList) {

        rvHospital.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener(){

                    @Override
                    public void onItemClick(View view, int position) {
                        String id= hospitalList.get(position).getId();
                        String nama_hospital = hospitalList.get(position).getNameHospital();
                        String location= hospitalList.get(position).getLocation();
                        String telepon = hospitalList.get(position).getTelepon();
                        String faximile = hospitalList.get(position).getFaximile();
                        String typeHospital = hospitalList.get(position).getTypeHospital();
                        String kodepos= hospitalList.get(position).getKodePos();
                        String email = hospitalList.get(position).getEmail();
                        String website = hospitalList.get(position).getWebsite();

                        Intent detail= new Intent(context, PuskesmasDetailActivity.class);
                        detail.putExtra(Constant.KEY_ID_HOSPITAL, id);
                        detail.putExtra(Constant.KEY_NAMA_HOSPITAL, nama_hospital);
                        detail.putExtra(Constant.KEY_LOCATION_HOSPITAL, location);
                        detail.putExtra(Constant.KEY_TELEPON_HOSPITAL, telepon);
                        detail.putExtra(Constant.KEY_FAXIMILE_HOSPITAL, faximile);
                        detail.putExtra(Constant.KEY_TYPE_HOSPITAL, typeHospital);
                        detail.putExtra(Constant.KEY_KODEPOS, kodepos);
                        detail.putExtra(Constant.KEY_EMAIL_HOSPITAL, email);
                        detail.putExtra(Constant.KEY_WEBSITE, website);
                        startActivity(detail);

                    }
                }));
    }
}


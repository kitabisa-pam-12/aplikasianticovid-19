package com.develops.groupproject.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.develops.groupproject.Database.Service;
import com.develops.groupproject.R;
import com.develops.groupproject.Util.UtilsApi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahPuskesmasActivity2 extends AppCompatActivity {

    EditText etNamaPuskesmas, etLocation, etTelepon, etFaximile, etKepalaPuskesmas, etAlamat, etEmail;
    Button btnSimpan;
    ProgressDialog loading;

    Service service;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_puskesmas_2);


        etNamaPuskesmas = (EditText) findViewById(R.id.etNamaPuskesmas);
        etLocation = (EditText) findViewById(R.id.etLocation);
        etTelepon= (EditText) findViewById(R.id.etTelepon);
        etFaximile=(EditText) findViewById(R.id.etFaximile);
        etKepalaPuskesmas= (EditText) findViewById(R.id.etKepalaPuskesmas);
        etAlamat=(EditText) findViewById(R.id.etAlamat);
        etEmail=(EditText) findViewById(R.id.etEmail);
        btnSimpan= (Button) findViewById(R.id.btnSimpan);
        context=this;
        service= UtilsApi.getAPIService();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestSimpanPuskesmas();

            }
        });

    }

    private void requestSimpanPuskesmas() {
        loading = ProgressDialog.show(context, null,"Harap Tunggu...", true, false);

        service.simpanPuskesmasRequest(
                etNamaPuskesmas.getText().toString(),
                etLocation.getText().toString(),
                etTelepon.getText().toString(),
                etFaximile.getText().toString(),
                etKepalaPuskesmas.getText().toString(),
                etAlamat.getText().toString(),
                etEmail.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    loading.dismiss();
                    Toast.makeText(context, "Data berhasil ditambahkan", Toast.LENGTH_LONG).show();
                }else {
                    loading.dismiss();
                    Toast.makeText(context, "Gagaal Menyimpan data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(context, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
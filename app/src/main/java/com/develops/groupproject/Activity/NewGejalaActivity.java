package com.develops.groupproject.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.develops.groupproject.R;


public class NewGejalaActivity extends AppCompatActivity {

    public static final String NOMOR_GEJALA = "nomor_gejala";
    public static final String TITLE_GEJALA = "title_gejala";
    public static final String KETERANGAN = "keterangan";

    private  EditText edit_no, edit_title, edit_keterangan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_gejala);
        edit_no = findViewById(R.id.edit_no);
        edit_title = findViewById(R.id.edit_title);
        edit_keterangan = findViewById(R.id.edit_keterangan);


        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(edit_no.getText())
                        || TextUtils.isEmpty(edit_title.getText()) || TextUtils.isEmpty(edit_keterangan.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String nomor = edit_no.getText().toString();
                    String title = edit_title.getText().toString();
                    String keterangan = edit_keterangan.getText().toString();

                    replyIntent.putExtra(NOMOR_GEJALA, nomor);
                    replyIntent.putExtra(TITLE_GEJALA, title);
                    replyIntent.putExtra(KETERANGAN, keterangan);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}


package com.develops.groupproject.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;

import com.develops.groupproject.Adapter.SurveiAdapter;
import com.develops.groupproject.Models.ModelSurveiMandiri;
import com.develops.groupproject.R;
import com.develops.groupproject.Repositori.SurveiRepositori;

import java.util.ArrayList;
import java.util.List;

public class ViewSurveiActivity extends AppCompatActivity {

    private static RecyclerView.Adapter adapter;
    private  RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private ArrayList<ModelSurveiMandiri> modelSurveiMandiri= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_survei);

        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        new LoadDataTask().execute();
    }


    class LoadDataTask extends AsyncTask<Void, Void, Void>{

        SurveiRepositori surveiRepositori;
        List<ModelSurveiMandiri> surveiList;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
//            surveiList= surveiRepositori.getSurvei();
            modelSurveiMandiri= new ArrayList<>();

            for (int i=0; i<surveiList.size(); i++){
                modelSurveiMandiri.add(surveiList.get(i));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);

//            SurveiAdapter surveiAdapter= new SurveiAdapter(modelSurveiMandiri, ViewSurveiActivity.this);
//            recyclerView.setAdapter(surveiAdapter);
        }

    }
}

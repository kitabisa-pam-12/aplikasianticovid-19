package com.develops.groupproject.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Models.ModelPuskesmas;
import com.develops.groupproject.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PuskesmasAdapter extends RecyclerView.Adapter<PuskesmasAdapter.PuskesmasHolder>{

    Context mContext;
    List<ModelPuskesmas> modelPuskesmaslist= new ArrayList<>();


    public PuskesmasAdapter(Context context, List<ModelPuskesmas> puskesmasList) {
        this.mContext = context;
        modelPuskesmaslist = puskesmasList;
    }


    @Override
    public PuskesmasAdapter.PuskesmasHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_puskesmas, parent,false);
        return new PuskesmasHolder(itemView);
    }

    @Override
    public void onBindViewHolder (PuskesmasAdapter.PuskesmasHolder holder, int position) {

        final ModelPuskesmas modelPuskesmas = modelPuskesmaslist.get(position);
        holder.tv_namaPuskesmas.setText(modelPuskesmas.getNamaPuskesmas());
        holder.tv_location.setText(modelPuskesmas.getLocation());



    }

    @Override
    public int getItemCount() {
        return modelPuskesmaslist.size();
    }

    public class PuskesmasHolder extends RecyclerView.ViewHolder {

        ImageView tvImg;
        TextView tv_namaPuskesmas;
        TextView tv_location;

        public PuskesmasHolder(View itemView) {
            super(itemView);

            tvImg = (ImageView)itemView.findViewById(R.id.imgPuskesmas);
            tv_namaPuskesmas = (TextView)itemView.findViewById(R.id.tv_nama_puskesmas);
            tv_location =(TextView)itemView.findViewById(R.id.tv_location);

        }
    }


}

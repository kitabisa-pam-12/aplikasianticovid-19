package com.develops.groupproject.Adapter;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.develops.groupproject.Models.ModelSurveiMandiri;
import com.develops.groupproject.Repositori.SurveiRepositori;

import java.util.List;

public class SurveiViewModel extends AndroidViewModel {

    private SurveiRepositori surveiRepositori;
    private LiveData<List<ModelSurveiMandiri>> modelSurveiMandiri;


    public SurveiViewModel(@NonNull Application application) {
        super(application);
        surveiRepositori = new SurveiRepositori(application);
        modelSurveiMandiri= surveiRepositori.getAll();

    }

    public void insertTask(ModelSurveiMandiri modelSurveiMandiri){
        surveiRepositori.insertTask(modelSurveiMandiri);
    }

    public void updateTask(ModelSurveiMandiri modelSurveiMandiri){
        surveiRepositori.updateTask(modelSurveiMandiri);
    }

    public void deleteTask(ModelSurveiMandiri modelSurveiMandiri){
        surveiRepositori.deleteTask(modelSurveiMandiri);
    }

    public void deeleteAllData(){
        surveiRepositori.deleteAllData();
    }

    public LiveData<List<ModelSurveiMandiri>> getAllData(){
        return modelSurveiMandiri;
    }
}

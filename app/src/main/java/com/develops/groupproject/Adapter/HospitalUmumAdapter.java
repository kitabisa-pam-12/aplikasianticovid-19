package com.develops.groupproject.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Models.ModelHospital;
import com.develops.groupproject.R;

import java.util.List;

public class HospitalUmumAdapter extends RecyclerView.Adapter<HospitalUmumAdapter.HospitalHolder>{

    Context context;
    List<ModelHospital> modelHospitallist;

    public HospitalUmumAdapter(Context context, List<ModelHospital> modelHospitallist) {
        this.context = context;
        this.modelHospitallist = modelHospitallist;
    }

    @Override
    public HospitalUmumAdapter.HospitalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_rsu, parent,false);
        return new HospitalUmumAdapter.HospitalHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HospitalUmumAdapter.HospitalHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return modelHospitallist.size();
    }

    public class HospitalHolder extends RecyclerView.ViewHolder {

        ImageView tvImg;
        TextView tv_namaHospital;
        TextView tv_location;

        public HospitalHolder(View itemView) {
            super(itemView);

            tvImg = (ImageView)itemView.findViewById(R.id.imgRs);
            tv_namaHospital = (TextView)itemView.findViewById(R.id.tv_nama_rs);
            tv_location =(TextView)itemView.findViewById(R.id.tv_location);

        }
    }


}

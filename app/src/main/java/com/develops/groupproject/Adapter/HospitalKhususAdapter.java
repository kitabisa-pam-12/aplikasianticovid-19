package com.develops.groupproject.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Models.ModelHospital;
import com.develops.groupproject.R;

import java.util.List;

public  class HospitalKhususAdapter extends RecyclerView.Adapter<HospitalKhususAdapter.HospitalHolder>{

    Context context;
    List<ModelHospital> modelHospitalList;

    public HospitalKhususAdapter(Context context, List<ModelHospital> hospitalList) {
        this.context = context;
        this.modelHospitalList = hospitalList;
    }

    @Override
    public HospitalKhususAdapter.HospitalHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_rsk, parent, false);
        return new HospitalHolder(itemView);
    }

    @Override
    public void onBindViewHolder( HospitalKhususAdapter.HospitalHolder holder, int position) {

        final ModelHospital modelHospital = modelHospitalList.get(position);
        holder.tvNamaHospital.setText(modelHospital.getNameHospital());
        holder.tvLocation.setText(modelHospital.getLocation());
    }

    @Override
    public int getItemCount() {
        return modelHospitalList.size();
    }

    public class HospitalHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tvNamaHospital;
        TextView tvLocation;


        public HospitalHolder( View itemView) {
            super(itemView);

            imageView=(ImageView)itemView.findViewById(R.id.imgRs);
            tvNamaHospital=(TextView)itemView.findViewById(R.id.tv_nama_rs);
            tvLocation=(TextView) itemView.findViewById(R.id.tv_location);

        }
    }
}
package com.develops.groupproject.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Models.Gejala;
import com.develops.groupproject.R;

import java.util.List;


public class GejalaListAdapter extends RecyclerView.Adapter<GejalaListAdapter.WordViewHolder> {

    class WordViewHolder extends RecyclerView.ViewHolder {
        private final TextView nomor, title, keterangan;

        private WordViewHolder(View itemView) {
            super(itemView);
            nomor = itemView.findViewById(R.id.tv_nomor);
            title = itemView.findViewById(R.id.tv_title);
            keterangan = itemView.findViewById(R.id.tv_note);
        }
    }

    private final LayoutInflater mInflater;
    private List<Gejala> mGejalas;

    public GejalaListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        Gejala current = mGejalas.get(position);
        holder.nomor.setText(current.getId());
        holder.title.setText(current.getTitle());
        holder.keterangan.setText(current.getKeterangan());
    }

    public void setWords(List<Gejala> gejalas){
        mGejalas = gejalas;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mGejalas != null)
            return mGejalas.size();
        else return 0;
    }
}



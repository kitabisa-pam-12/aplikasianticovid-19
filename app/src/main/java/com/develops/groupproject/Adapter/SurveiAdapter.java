package com.develops.groupproject.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.develops.groupproject.Models.ModelSurveiMandiri;
import com.develops.groupproject.R;

import java.util.ArrayList;
import java.util.List;

public class SurveiAdapter extends RecyclerView.Adapter<SurveiAdapter.MyViewHolder> {

    private List<ModelSurveiMandiri> modelSurveiMandiri = new ArrayList<>();
    private OnItemClickListener listener;


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_survei, parent, false);
                return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       ModelSurveiMandiri current = modelSurveiMandiri.get(position);
        holder.tv_nik.setText(current.getNik());
        holder.tv_nama.setText(current.getNama());
        holder.tv_ttl.setText(current.getTtl());
        holder.tv_alamat.setText(current.getAlamat());
        holder.tv_email.setText(current.getEmail());
        holder.tv_kontak.setText(current.getKontak());
        holder.tv_gender.setText(current.getGender());

    }

    @Override
    public int getItemCount() {
        return modelSurveiMandiri.size();
    }

     class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_no, tv_nik, tv_gender,tv_nama, tv_ttl,  tv_alamat, tv_email,tv_kontak;
        ImageView  img_call;
        Button btn_title;

        public MyViewHolder( View itemView) {
            super(itemView);
            this.tv_nik=(TextView)itemView.findViewById(R.id.tv_nik);
            this.tv_nama=(TextView)itemView.findViewById(R.id.tv_nama);
            this.tv_ttl=(TextView)itemView.findViewById(R.id.tv_ttl);
            this.tv_alamat=(TextView)itemView.findViewById(R.id.tv_alamat);
            this.tv_kontak=(TextView) itemView.findViewById(R.id.tv_kontak);
            this.tv_email=(TextView) itemView.findViewById(R.id.tv_email);
            this.tv_gender=(TextView) itemView.findViewById(R.id.tv_gender);

            itemView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    int position =  getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION ){
                    listener.onItemClick(modelSurveiMandiri.get(position));
                }}
            });
        }
    }

    public void setSurvei(List<ModelSurveiMandiri> modelSurveiMandiri){
        this.modelSurveiMandiri= modelSurveiMandiri;
        notifyDataSetChanged();
    }
    public ModelSurveiMandiri getSurvei(int position){
        return modelSurveiMandiri.get(position);
    }

    public  interface OnItemClickListener{
        void onItemClick(ModelSurveiMandiri surveiMandiri);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;

    }
}

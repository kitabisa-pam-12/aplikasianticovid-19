package com.develops.groupproject.Util;


import com.develops.groupproject.Database.Service;

public class UtilsApi {

    public static final String BASE_URL_API = "http://192.168.43.121:81/Covid19/";


    public static Service getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(Service.class);
    }
}
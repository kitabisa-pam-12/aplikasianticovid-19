package com.develops.groupproject.Util;

public class Constant {
    public static final String KEY_NAMA_PUSKESMAS= "keyNamaPuskesmas";
    public static final String KEY_LOCATION= "keyLocation";
    public static final String KEY_TELEPON= "keyTelepon";
    public static final String KEY_FAXIMILE= "keyFaximile";
    public static final String KEY_KEPALA_PUSKESMAS= "keyKepalaPuskesmas";
    public static final String KEY_ALAMAT= "keyAlamat";
    public static final String KEY_EMAIL= "keyEmail";
    public static final String KEY_ID ="keyId" ;


    public static final String KEY_NAMA_HOSPITAL= "keyNamaHospitals";
    public static final String KEY_LOCATION_HOSPITAL= "keyLocationHospital";
    public static final String KEY_TELEPON_HOSPITAL= "keyTeleponHospital";
    public static final String KEY_FAXIMILE_HOSPITAL= "keyFaximileHospital";
    public static final String KEY_WEBSITE= "keyWebsite";
    public static final String KEY_KODEPOS= "keyKodePos";
    public static final String KEY_EMAIL_HOSPITAL= "keyEmailGHospital";
    public static final String KEY_ID_HOSPITAL ="keyIdHospital" ;
    public static final String KEY_TYPE_HOSPITAL ="keyTypeHospital" ;
}


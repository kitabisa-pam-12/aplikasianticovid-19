package com.develops.groupproject.Repositori;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.room.Update;

import com.develops.groupproject.Database.SurveiDao;
import com.develops.groupproject.Database.SurveiDatabase;
import com.develops.groupproject.Models.ModelSurveiMandiri;

import java.util.List;

public class SurveiRepositori {


    private SurveiDao surveiDao;
    private LiveData<List<ModelSurveiMandiri>> modelSurveiMandiri;
    Context context;

    public SurveiRepositori(Application application){
        SurveiDatabase database = SurveiDatabase.getInstance(application);
        surveiDao =  database.surveiDao();
        modelSurveiMandiri = surveiDao.getAll();

}

   public void insertTask(ModelSurveiMandiri modelSurveiMandiri){

        new InsertSurveiAsyncTask(surveiDao).execute(modelSurveiMandiri);
   }

    public void updateTask(ModelSurveiMandiri modelSurveiMandiri){
        new UpdateSurveiAsyncTask(surveiDao).execute(modelSurveiMandiri);

    }
    public void deleteTask(ModelSurveiMandiri modelSurveiMandiri){
        new DeleteSurveiAsyncTask(surveiDao).execute(modelSurveiMandiri);

    }
    public void deleteAllData(){
        new DeleteAllSurveiAsyncTask(surveiDao).execute();

    }
    public LiveData<List<ModelSurveiMandiri>> getAll(){
        return modelSurveiMandiri;
    }

    private static class InsertSurveiAsyncTask extends AsyncTask<ModelSurveiMandiri, Void, Void>{
        private SurveiDao surveiDao;

        private InsertSurveiAsyncTask(SurveiDao surveiDao){
            this.surveiDao=surveiDao;
        }

        @Override
        protected Void doInBackground(ModelSurveiMandiri... modelSurveiMandiris) {
           surveiDao.inserTask(modelSurveiMandiris[0]);
            return null;
        }
    }

    private static class UpdateSurveiAsyncTask extends AsyncTask<ModelSurveiMandiri, Void, Void>{
        private SurveiDao surveiDao;

        private UpdateSurveiAsyncTask(SurveiDao surveiDao){
            this.surveiDao=surveiDao;
        }

        @Override
        protected Void doInBackground(ModelSurveiMandiri... modelSurveiMandiris) {
            surveiDao.updateTask(modelSurveiMandiris[0]);
            return null;
        }
    }

    private static class DeleteSurveiAsyncTask extends AsyncTask<ModelSurveiMandiri, Void, Void>{
        private SurveiDao surveiDao;

        private DeleteSurveiAsyncTask(SurveiDao surveiDao){
            this.surveiDao=surveiDao;
        }

        @Override
        protected Void doInBackground(ModelSurveiMandiri... modelSurveiMandiris) {
            surveiDao.deleteTask(modelSurveiMandiris[0]);
            return null;
        }
    }

    private static class DeleteAllSurveiAsyncTask extends AsyncTask<Void, Void, Void>{
        private SurveiDao surveiDao;

        private DeleteAllSurveiAsyncTask(SurveiDao surveiDao){
            this.surveiDao=surveiDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            surveiDao.deleteAllData();
            return null;
        }
    }
}

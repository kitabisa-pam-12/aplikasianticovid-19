package com.develops.groupproject.Repositori;


import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.develops.groupproject.Database.GejalaDao;
import com.develops.groupproject.Database.GejalaRoomDatabase;
import com.develops.groupproject.Models.Gejala;

import java.util.List;

public class GejalaRepository {

    private GejalaDao mGejalaDao;
    private LiveData<List<Gejala>> mAllWords;

    public GejalaRepository(Application application) {
        GejalaRoomDatabase db = GejalaRoomDatabase.getDatabase(application);
        mGejalaDao = db.wordDao();
        mAllWords = mGejalaDao.getAlphabetizedWords();
    }

    public LiveData<List<Gejala>> getAllWords() {
        return mAllWords;
    }

    public void insert (Gejala gejala) {
        new insertAsyncTask(mGejalaDao).execute(gejala);
    }

    private static class insertAsyncTask extends AsyncTask<Gejala, Void, Void> {

        private GejalaDao mAsyncTaskDao;

        insertAsyncTask(GejalaDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Gejala... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}

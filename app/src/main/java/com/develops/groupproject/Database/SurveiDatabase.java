package com.develops.groupproject.Database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.develops.groupproject.Models.ModelSurveiMandiri;

@Database(entities = {ModelSurveiMandiri.class}, version = 1, exportSchema = false)
public abstract class SurveiDatabase extends RoomDatabase {

    public static SurveiDatabase instance;
    public abstract SurveiDao surveiDao();
    public static synchronized SurveiDatabase getInstance(Context context){
        if (instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),
                    SurveiDatabase.class, "survei_table")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }

        return  instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db){
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void>{

        private SurveiDao surveiDao;
        private PopulateDbAsyncTask(SurveiDatabase db){
            surveiDao= db.surveiDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            surveiDao.inserTask(new ModelSurveiMandiri( "11425266", "grace sitorus", "porsea, 01 juni 2000", "perempuan","Porsea", "0866554", "grace@gmail.com"));
            return null;
        }
    }
}

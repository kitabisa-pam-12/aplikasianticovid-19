package com.develops.groupproject.Database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.develops.groupproject.Models.Gejala;



@Database(entities = {Gejala.class}, version = 2)
public abstract class GejalaRoomDatabase extends RoomDatabase {

    public abstract GejalaDao wordDao();

    private static GejalaRoomDatabase INSTANCE;

    public static GejalaRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (GejalaRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            GejalaRoomDatabase.class, "gejala_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){

        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final GejalaDao mDao;

        PopulateDbAsync(GejalaRoomDatabase db) {
            mDao = db.wordDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mDao.deleteAll();

            Gejala gejala = new Gejala("1", "Demam", "Satu diantara gejala terinfeksi viruss corona yang akan muncul adalah demam tinggi, mirip saat terkena flu.");
            mDao.insert(gejala);
            return null;
        }
    }

}

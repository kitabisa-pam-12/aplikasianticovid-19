package com.develops.groupproject.Database;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.develops.groupproject.Models.ModelSurveiMandiri;

import java.util.List;

@Dao
public interface SurveiDao {

    @Insert
    Long inserTask(ModelSurveiMandiri modelSurveiMandiri);

    @Update
    void updateTask(ModelSurveiMandiri modelSurveiMandiri);

    @Delete
    void deleteTask(ModelSurveiMandiri modelSurveiMandiri);

    @Query("select * from survei_table order by id asc ")
    LiveData<List<ModelSurveiMandiri>> getAll();

    @Query("delete from survei_table ")
    void deleteAllData();
}

package com.develops.groupproject.Database;

import com.develops.groupproject.Models.ModelDataIndonesia;
import com.develops.groupproject.Models.ResponseHospital;
import com.develops.groupproject.Models.ResponsePuskesmas;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Service {

    @GET("indonesia")
    Call<List<ModelDataIndonesia>> getData();


    @GET("puskesmas")
    Call<ResponsePuskesmas> getSemuaPuskesmas();

    @FormUrlEncoded
    @POST("puskesmas")
    Call<ResponseBody> simpanPuskesmasRequest(@Field("namaPuskesmas") String namaPuskesmas,
                                           @Field("location") String location,
                                            @Field("telepon") String telepon,
                                            @Field("faximile") String faximile,
                                            @Field("kepalaPuskesmas") String kepalaPuskesmas,
                                            @Field("alamat") String alamat,
                                            @Field("email") String email);

    @DELETE("puskesmas/{id}")
    Call<ResponseBody> detelePuskesmas(@Path("id") String id);

    @GET("hospital")
    Call<ResponseHospital> getSemuaHospital();

    @FormUrlEncoded
    @POST("hospital")
    Call<ResponseBody> simpanPuskesmasHospital(@Field("nameHospital") String nameHospital,
                                              @Field("location") String location,
                                              @Field("typeHospital") String typeHospital,
                                               @Field("kodePos") String kodePos,
                                               @Field("telepon") String telepon,
                                              @Field("email") String email,
                                              @Field("faximile") String faximile,
                                              @Field("website") String website);

    @DELETE("hospital/{id}")
    Call<ResponseBody> deteleHospital(@Path("id") String id);


}

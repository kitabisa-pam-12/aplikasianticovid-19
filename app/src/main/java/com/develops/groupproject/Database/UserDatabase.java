package com.develops.groupproject.Database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.develops.groupproject.Models.User;

@Database(entities = {User.class}, version = 1, exportSchema = false)

public abstract class UserDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

}

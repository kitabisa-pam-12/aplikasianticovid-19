package com.develops.groupproject.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.develops.groupproject.Models.Gejala;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;




@Dao
public interface GejalaDao {

    @Query("SELECT * from gejala_table ORDER BY id ASC")
    LiveData<List<Gejala>> getAlphabetizedWords();

    @Insert
    void insert(Gejala gejala);

    @Query("DELETE FROM gejala_table")
    void deleteAll();
}

package com.example.pengecekansurveymandiri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ViewCekResiko extends AppCompatActivity {

    private  String[] pasien = { "Gita","Grace", "Veby", "Lamtiur","Miranti", "Lamsihar", "Bongson","Hengki"};
    private ArrayList<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cek_resiko);
        ListView listView = findViewById(R.id.list);
        data = new ArrayList<>();
        getData();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, data);
        listView.setAdapter(adapter);


    }

    private void getData(){
        Collections.addAll(data, pasien );
    }
}
